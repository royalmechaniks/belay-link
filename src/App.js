import './App.css';
import ActivityInfo from './components/ActivityInfo';
import Header from './components/Header';
import Navbar from './components/Navbar';
import Popup from './components/Popup';
import UserInfo from './components/UserInfo';
import { useCallback, useEffect, useState } from 'react';

function App() {
  const [isPopupOpened, setIsPopupOpened] = useState(false);
  const [popupText, setPopupText] = useState('');
  const [completeButtonClicked, setCompleteButtonClicked] = useState(false);
  const [addButtonClicked, setAddButtonClicked] = useState(false);

  const togglePopup = useCallback(
    (e) => {
      e.stopPropagation();
      setIsPopupOpened(!isPopupOpened);

      if (completeButtonClicked) {
        setCompleteButtonClicked(false);
      }
      if (addButtonClicked) {
        setAddButtonClicked(false);
      }
    },
    [isPopupOpened, completeButtonClicked, addButtonClicked]
  );

  useEffect(() => {
    if (isPopupOpened) {
      document.addEventListener('click', togglePopup);
    }
  }, [isPopupOpened, togglePopup]);

  return (
    <div className='App'>
      <div className='app-container'>
        <Header />
        <UserInfo />
        <ActivityInfo
          togglePopup={togglePopup}
          setPopupText={setPopupText}
          setCompleteButtonClicked={setCompleteButtonClicked}
          completeButtonClicked={completeButtonClicked}
        />
        <Navbar
          togglePopup={togglePopup}
          setPopupText={setPopupText}
          setAddButtonClicked={setAddButtonClicked}
          addButtonClicked={addButtonClicked}
        />
        {isPopupOpened && <Popup text={popupText} addButtonClicked={addButtonClicked} />}
      </div>
    </div>
  );
}

export default App;
