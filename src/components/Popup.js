import PopupBase from '../assets/images/Popover Base.svg';
import './Popup.css';

const Popup = ({ text, addButtonClicked }) => {
  return (
    <div className='popup-wrapper'>
      <div className={['popup', addButtonClicked ? 'popup-navbar' : ''].join(' ').trim()}>
        <img src={PopupBase} alt='popup' />
        <p>{text}</p>
      </div>
    </div>
  );
};

export default Popup;
