import Menu from '../assets/icons/menu.svg';
import BelayLinkLogo from '../assets/images/BelayLink.svg';
import UserAvatar from '../assets/images/UserAvatar.svg';
import Message from '../assets/icons/message-square.svg';
import Bell from '../assets/icons/bell.svg';

import './Header.css';

const Header = () => {
  return (
    <header>
      <ul>
        <li>
          <img src={Menu} alt='menu button' />
        </li>
        <li>
          <img src={BelayLinkLogo} alt='belay link logo' />
        </li>
      </ul>
      <ul className='icons-list'>
        <li>
          <img src={UserAvatar} alt='user avatar' />
        </li>
        <li>
          <img src={Message} alt='message icon' />
        </li>
        <li className='notifications-icon'>
          <img src={Bell} alt='notifications icon' />
          <div className='icon-active'></div>
        </li>
      </ul>
    </header>
  );
};

export default Header;
