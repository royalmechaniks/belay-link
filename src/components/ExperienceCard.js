import './ExperienceCard.css';

const ExperienceCard = () => {
  return (
    <div className='container'>
      <div className='experience'>
        <span className='level-pill'>Advanced</span>
        <p>
          <span className='text-huge'>10</span> yrs
        </p>
      </div>
      <div className='skills'>
        <div>
          <span>Lead Climbing</span>
          <div className='skill-point red'></div>
          <div className='skill-point red'></div>
          <div className='skill-point red'></div>
          <div className='skill-point'></div>
        </div>
        <div>
          <span>Bouldering</span>
          <div className='skill-point yellow'></div>
          <div className='skill-point yellow'></div>
          <div className='skill-point'></div>
          <div className='skill-point'></div>
        </div>
        <div>
          <span>Trad Climbing</span>
          <div className='skill-point blue'></div>
          <div className='skill-point'></div>
          <div className='skill-point'></div>
          <div className='skill-point'></div>
        </div>
      </div>
    </div>
  );
};

export default ExperienceCard;
