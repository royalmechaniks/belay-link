import './Navbar.css';
import Lurking from '../assets/icons/lurking.svg';
import Message from '../assets/icons/message-gray.svg';

const Navbar = ({ setPopupText, togglePopup, setAddButtonClicked, addButtonClicked }) => {
  const handleClick = () => {
    alert('Not connected to the endpoint yet...');
  };

  const text = 'Now let’s see how you can find people to do sports together!';

  const handleAdd = (e) => {
    setAddButtonClicked(true);
    togglePopup(e);
    setPopupText(text);
  };

  return (
    <nav>
      <ul className={addButtonClicked ? 'active' : ''}>
        <li>
          <button onClick={handleClick}>
            <img src={Lurking} alt='lurking eyes icon' />
            <span>Look around</span>
          </button>
        </li>
        <li>
          <button className='btn-add' onClick={handleAdd}>
            +
          </button>
        </li>
        <li>
          <button className='btn-message' onClick={handleClick}>
            <img src={Message} alt='message icon' />
            <span>Message</span>
            <div className='message-active'></div>
          </button>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
