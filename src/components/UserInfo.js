import './UserInfo.css';
import Avatar from '../assets/images/User.svg';
import Cake from '../assets/icons/cake-icon.svg';
import Location from '../assets/icons/location-icon.svg';
import Gender from '../assets/icons/gender-icon.svg';
import Eye from '../assets/icons/eye.svg';
import Share from '../assets/icons/share-icon.svg';

const UserInfo = () => {
  return (
    <div className='user-info'>
      <div className='user-name'>
        <img src={Avatar} alt='user avatar' />
        <h4>Mandy Chen</h4>
      </div>
      <div className='user-details'>
        <div>
          <div>
            <img src={Cake} alt='cake icon' />
            <span>34</span>
          </div>
          <div>
            <img src={Location} alt='location icon' />
            <span>Taipei</span>
          </div>
          <div>
            <img src={Gender} alt='gender icon' />
            <span>F</span>
          </div>
        </div>

        <button>
          <img src={Share} alt='share icon' />
          <span>Share my account</span>
        </button>
      </div>

      <img src={Eye} alt='eye icon' className='visibility-icon' />
    </div>
  );
};

export default UserInfo;
