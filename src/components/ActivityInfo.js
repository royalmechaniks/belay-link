import './ActivityInfo.css';
import ExperienceCard from './ExperienceCard';

const ActivityInfo = ({
  togglePopup,
  setPopupText,
  setCompleteButtonClicked,
  completeButtonClicked,
}) => {
  const text = 'You can make your sports profile look better later :)';

  const handleClick = (e) => {
    setCompleteButtonClicked(true);
    togglePopup(e);
    setPopupText(text);
  };

  return (
    <div className='activity-info'>
      <ExperienceCard />
      <div className='activity-text'>
        <p>When I usually go</p>
        <div>
          <span>Weekday evening</span>
          <span>Weekday morning</span>
          <span>Weekend</span>
        </div>
        <p>Where I usually go and want to go</p>
        <div>
          <span>Tup Zhonghe</span>
          <span>Long Dong</span>
        </div>
        <button className={completeButtonClicked ? 'active' : ''} onClick={handleClick}>
          Complete my sports profile
        </button>
      </div>
      <div className='activity-name'>
        <span>Rock Climbing</span>
      </div>
    </div>
  );
};

export default ActivityInfo;
